package com.example.preliminaryfaa.service;

import com.example.preliminaryfaa.component.DateUtils;
import com.example.preliminaryfaa.model.ProceduresResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FileParserService {
    @Autowired
    private DateUtils dateUtils;

    public ProceduresResponse readTextFromPdfFile(Set<String> set) throws IOException {
        File dataDirectory = new File("./data");
        Set<String> filteredProcedures = new HashSet<>(set);
        List<String> copyarray = new ArrayList<>(set);

        for (File pdfFile : Objects.requireNonNull(dataDirectory.listFiles())) {
            if (!pdfFile.getName().endsWith(".pdf") || (!pdfFile.getName().startsWith("nfdd") && !pdfFile.getName().startsWith("transmittal_letter"))) {
//                log.info("Omijam plik:" + pdfFile.getName());
                continue;
            }

            if (!dateUtils.isInCycle(pdfFile.getName())) {
//                log.info("Omijam plik (poza cyklem):" + pdfFile.getName());
//                pdfFile.delete();
                continue;
            }

            PDDocument document = PDDocument.load(new File("data/" + pdfFile.getName()));
            String[] podzielone = {""};
            int counter = 0;

            if (!document.isEncrypted()) {
                PDFTextStripper stripper = new PDFTextStripper();
                String text = stripper.getText(document);
//                if(text.contains("NUMBER") || text.contains("NAME")){
//                    System.out.println("found it");
//                }
                String[] slowa = text.split(" ");


                for (String slowo : slowa) {
                    for (int i = 0; i < (set.size()); i++) {
//                        if(slowo.contains("NUMBER") || slowo.contains("NAME")){
//                            System.out.println("found it");
//                        }
                        if (slowo.contains("\n") && slowo.contains(copyarray.get(i))) {
                            podzielone = slowo.split("\n");
                            for (int j = 0; j < podzielone.length; j++) {

                                if ((podzielone[j].contains("Date") || podzielone[j].contains("Name") || podzielone[j].contains("NAME")) &&
                                        podzielone[1].contains(copyarray.get(i))) {

                                    filteredProcedures.remove(copyarray.get(i));
                                    counter++;
                                    System.out.println(copyarray.get(i));
                                }
                            }
                        }
                    }
                }

            }
            document.close();
            System.out.println(counter);
        }

        System.out.println("Filtered published: " + filteredProcedures);
        System.out.println("FAA published " + (set.size() - filteredProcedures.size()) +
                " published which is " + (set.size() - filteredProcedures.size()) * 100 / set.size() + "% of total expected published");

        Set<String> going = new HashSet<>(set);
        going.removeAll(filteredProcedures);

        Map<String, String> proceduresResponseMap = corelate(going, new BufferedReader(new FileReader(new File("data/prelim.csv"))).lines().collect(Collectors.toList()));
        return new ProceduresResponse(proceduresResponseMap, going, filteredProcedures);
    }

    private Map<String, String> corelate(Set<String> filteredProcedures, List<String> fileLines) {

        return filteredProcedures.stream().collect(Collectors.toMap(
                o -> o,
                o -> fileLines.stream().filter(w -> w.contains(String.valueOf(o))).findFirst().get()));
    }

    public Set<String> readFile(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        List<String> array = new ArrayList<>();
        int counter = 0;
//        Poniżej usuwam przedrostki SID i STAR oraz wszystkie wyrazy RNAV i OBSTACLE, a także wszystkie nawiasy.
//        W związku z tym, że czasami występuję więcej niż jedna spacja pomiędzy wyrazami zastosowałem zamianę pustych
//        znaków na dokładnie jeden pusty znak


        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
//          Tutaj pozbywam się wszsytkich linii zawierających ORIG lub AMDT oraz pierwszą linię tytułową poprzez wywołanie
//          dowolnego pola.
            if (line.contains("ORIG") || line.contains("AMDT") || line.contains("ICAO ID")) {
                continue;
            }
            line = line.replaceAll("[0-9]", "");
            line = line.replaceAll("SID", "");
            line = line.replaceAll("STAR", "");
            line = line.replaceAll("\\(", "");
            line = line.replaceAll("\\)", "");
            line = line.replaceAll("OBSTACLE", "");
            line = line.replaceAll("RNAV", "");
            line = line.replaceAll("\\s+", " ");
//            Następnie zamieniam wszystkie wyrazy na cyfry
            if (line.contains(" ONE ")) line = line.replaceAll(" ONE ", "1 ");
            else if (line.contains(" TWO ")) line = line.replaceAll(" TWO ", "2 ");
            else if (line.contains(" THREE ")) line = line.replaceAll(" THREE ", "3 ");
            else if (line.contains(" FOUR ")) line = line.replaceAll(" FOUR ", "4 ");
            else if (line.contains(" FIVE ")) line = line.replaceAll(" FIVE ", "5 ");
            else if (line.contains(" SIX ")) line = line.replaceAll(" SIX ", "6 ");
            else if (line.contains(" SEVEN ")) line = line.replaceAll(" SEVEN ", "7 ");
            else if (line.contains(" EIGHT ")) line = line.replaceAll(" EIGHT ", "8 ");
            else if (line.contains(" NINE ")) line = line.replaceAll(" NINE ", "9 ");

//            Następnie zamieniam wszystkie stringi na tablicę do której dodaję wyrazy zawierające cyfry.
            line = line.replaceAll("\"", "");
            String[] slowa = line.split(" ");
            for (String slowo : slowa) {
                if (!slowo.contains("1") && !slowo.contains("2") && !slowo.contains("3") && !slowo.contains("4") && !slowo.contains("5")
                        && !slowo.contains("6") && !slowo.contains("7") && !slowo.contains("8") && !slowo.contains("9")) {
                    continue;
                }
//                if (slowo.length() == 5) {
//                    counter++;
//                    array.add(slowo);
//                    System.out.println(slowo);
//                } else {
                slowo = slowo.substring(0, slowo.length() - 1);
                counter++;
                array.add(slowo);
                System.out.println(slowo);
//                }
            }
        }
        System.out.println("FAA is going to publish " + counter + " published this cycle");
        return new HashSet<>(array);
    }
}
