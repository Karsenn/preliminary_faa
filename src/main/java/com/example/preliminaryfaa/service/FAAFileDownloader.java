package com.example.preliminaryfaa.service;

import com.example.preliminaryfaa.model.FlightInformation;
import com.example.preliminaryfaa.model.ProceduresResponse;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.util.List;

@Service
public class FAAFileDownloader {

    private final FileParserService fileParserService;

    @Autowired
    public FAAFileDownloader(FileParserService fileParserService) {
        this.fileParserService = fileParserService;
    }

    private void downloadCSVFile(int cycle) throws IOException {
        String tekstDoDodania = "";
        if (cycle == 1902) {
            tekstDoDodania = "9%2F10%2F2020%20%28CN%29%2C10%2F8%2F2020%20%28TPP%29";
        } else if (cycle == 1903) {
            tekstDoDodania = "7%2F18%2F2019%20%28TPP%29%2C8%2F15%2F2019%20%28CN%29";
        } else if (cycle == 1904) {
            tekstDoDodania = "11%2F5%2F2020%20%28CN%29%2C12%2F3%2F2020%20%28TPP%29";

        }

        URL url = new URL("https://www.faa.gov/air_traffic/flight_info/aeronav/procedures/application/index.cfm?event=procedure.exportResults&tab=productionPlan&publicationDate=" + tekstDoDodania);
        InputStream in = url.openStream();
        FileOutputStream fos = new FileOutputStream(new File("data/prelim.csv"));
        int length;
        byte[] buffer = new byte[1024];
        while ((length = in.read(buffer)) > -1) {
            fos.write(buffer, 0, length);
        }
    }

    public ProceduresResponse pullLatestFAAFiles(int numOfTransmittal, int numberOfNfdd, int cycles) throws IOException {
        try {
            downloadCSVFile(cycles);
        } catch (Exception e) {
            System.err.println("continue without csv");
        }
        downloadTransmitallLetterPdf(numOfTransmittal);
        downloadNfddPdf(numberOfNfdd);

        File file = new File("data/prelim.csv");
        return fileParserService.readTextFromPdfFile(fileParserService.readFile(file));
    }


    private void downloadNfddPdf(int numberOfNfdd) throws IOException {
        URL url = new URL("https://nfdc.faa.gov/nfdcApps/controllers/PublicDataController/getNfddListData?dataType=NFDD&start=0&length=" + numberOfNfdd + "&sortcolumn=effective_date&sortdir=desc");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try (InputStream inputStream = url.openStream()) {
            ObjectNode node = objectMapper.readValue(inputStream, ObjectNode.class);
            JsonNode data = node.get("data");

            JsonParser parser = objectMapper.treeAsTokens(data);
            TypeReference<List<FlightInformation>> rootType = new TypeReference<List<FlightInformation>>() {
            };
            List<FlightInformation> FlightInformations = objectMapper.readValue(parser, rootType);

            for (FlightInformation FlightInformation : FlightInformations) {
                System.out.println(FlightInformation.getEffectiveDate() + " : " + FlightInformation.getFileName());


                URL url1 = new URL("https://nfdc.faa.gov/webContent/nfdd/" + FlightInformation.getFileName());
                InputStream in = url1.openStream();
                FileOutputStream fos = new FileOutputStream(new File("data/" + FlightInformation.getFileName()));
                int length;
                byte[] buffer = new byte[1024];// buffer for portion of data from connection
                while ((length = in.read(buffer)) > -1) {
                    fos.write(buffer, 0, length);
                }
            }
            parser.close();
        }
    }

    private void downloadTransmitallLetterPdf(int numberOfTrasmittalLetter) throws IOException {

        URL url = new URL("https://nfdc.faa.gov/nfdcApps/controllers/PublicDataController/getTransmittalLetterListData?dataType=TRANSMITTALLETTER&start=0&length=" + numberOfTrasmittalLetter + "&sortcolumn=effective_date&sortdir=desc");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try (InputStream inputStream = url.openStream()) {
            ObjectNode node = objectMapper.readValue(inputStream, ObjectNode.class);
            JsonNode data = node.get("data");

            JsonParser parser = objectMapper.treeAsTokens(data);
            TypeReference<List<FlightInformation>> rootType = new TypeReference<List<FlightInformation>>() {
            };
            List<FlightInformation> transimittalLetters = objectMapper.readValue(parser, rootType);

            for (FlightInformation transimittalLetter : transimittalLetters) {
                System.out.println(transimittalLetter.getEffectiveDate() + " : " + transimittalLetter.getFileName());


                URL url1 = new URL("https://nfdc.faa.gov/webContent/transmittal_letter/" + transimittalLetter.getFileName());
                InputStream in = url1.openStream();
                FileOutputStream fos = new FileOutputStream(new File("data/" + transimittalLetter.getFileName()));
                int length;
                byte[] buffer = new byte[1024];// buffer for portion of data from connection
                while ((length = in.read(buffer)) > -1) {
                    fos.write(buffer, 0, length);
                }
            }
            parser.close();
        }
    }
}
