package com.example.preliminaryfaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PreliminaryfaaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreliminaryfaaApplication.class, args);
    }

}
