package com.example.preliminaryfaa.controller;

import com.example.preliminaryfaa.model.ProceduresResponse;
import com.example.preliminaryfaa.model.UserFilesFormRequest;
import com.example.preliminaryfaa.service.FAAFileDownloader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.Arrays;

@Controller
@RequestMapping
public class IndexController {

    private final FAAFileDownloader faaFileDownloader;

    @Autowired
    public IndexController(FAAFileDownloader faaFileDownloader) {
        this.faaFileDownloader = faaFileDownloader;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("request", new UserFilesFormRequest());
        return "home";
    }

    @PostMapping("/down")
    public String download(UserFilesFormRequest request, Model model) {
        try {
            ProceduresResponse proceduresResponse = faaFileDownloader.pullLatestFAAFiles(request.getNumOfTransmittal(), request.getNumOfNfdd(), request.getCycle());

            // TODO: przekazać z metody pull wstecz wartości aż z FAAFileDownloadera
            model.addAttribute("procedures", proceduresResponse);

            return "result";
        } catch (IOException e) {
            //todo: obsługa błędu
            model.addAttribute("message", e.getMessage());
            model.addAttribute("stack", Arrays.asList(e.getStackTrace()));
            return "error";
        }
    }

}
