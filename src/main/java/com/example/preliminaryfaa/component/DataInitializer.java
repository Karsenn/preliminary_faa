package com.example.preliminaryfaa.component;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;


@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {
        File dataFolder = new File("./data");
        if (!dataFolder.exists() || !dataFolder.isDirectory()) {
            if (dataFolder.mkdir()) {
                System.out.println("Directory created.");
            }
        }
    }
}
