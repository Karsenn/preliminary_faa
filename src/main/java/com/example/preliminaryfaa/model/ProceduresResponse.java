package com.example.preliminaryfaa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProceduresResponse {
    private Map<String, String> filteredProcedures;
    private Set<String> published;
    private Set<String> goingToPublish;
}
