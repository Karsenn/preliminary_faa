package com.example.preliminaryfaa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFilesFormRequest {
    @Min(1902)
    @Max(1904)
    private int cycle;

    @Min(value = 0)
    @Max(value = 4)
    private int numOfTransmittal;

    @Min(value = 0)
    @Max(value = 40)
    private int numOfNfdd;
}
